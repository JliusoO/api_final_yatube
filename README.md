# Api_final_yatube

## О проекте
Социальная сеть Yatube реализованная на REST API

## Зависимости
- Python 3.7 
- Django 2.2.19
- Djangorestframework 3.16

## Быстрый старт:
Клонировать репозиторий и перейти в него в командной строке:
```
git clone https://github.com/JliusoO/api_final_yatube.git
```
```
cd api_final_yatube
```
Cоздать и активировать виртуальное окружение:
```
python -m venv venv
```
```
source venv/bin/activate
```
Установить зависимости из файла requirements.txt:
```
python3 -m pip install --upgrade pip
```
```
pip install -r requirements.txt
```
Выполнить миграции:
```
python yatube_api/manage.py migrate
```
```
python yatube_api/manage.py makemigrations
```
Запустить проект:
```
python manage.py runserver
```
## Документация к API 
```
https://editor.swagger.io/
```
```
yatube_api/static/redoc.yaml
```
## Примеры запросов
### Получение JWT-токена.
POST http://127.0.0.1:8000/api/v1/jwt/create/ 
```
{
    "username": "string",
    "password": "string"
}
```
200:
```
{
    "refresh": "string",
    "access": "string"
}
```
### Получение публикации по id.
GET http://127.0.0.1:8000/api/v1/posts/{id}/
```
http://127.0.0.1:8000/api/v1/posts/2/ 
```
200:
```
{
  "id": 0,
  "author": "string",
  "text": "string",
  "pub_date": "2022-10-26T08:37:03.595Z",
  "image": "string",
  "group": 0
}
```
400:
```
{
  "detail": "Страница не найдена."
}
```
### Подписка пользователя от имени которого сделан запрос на пользователя переданного в теле запроса. Анонимные запросы запрещены.
POST http://127.0.0.1:8000/api/v1/follow/
```
{
  "following": "string"
}
```
201:
```
{
  "user": "string",
  "following": "string"
}
```

